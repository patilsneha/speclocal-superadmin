export const environment = {
  production: true,    
  //demo :-
  // baseUrl :  'https://speclocal.com:5500',
    baseUrl :  'https://speclocal.com:4500',   //live
  //main website :-
  //  baseUrl :  'http://139.59.43.12:4500',
  // baseUrl :  'http://localhost:5500',
   count_of_unapproved_msg_credit : '/msgcredit/count',
   all_user_unapproved_msg_credit : '/msgcredit/getalldata',
   send_message_credit_to_user : '/msgcredit/updatecredit'
};
