// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  ///for demo:-
  // baseUrl :  'https://speclocal.com:5500',

  //for localhost  :-
  baseUrl :  'http://localhost:4500',
  //live
  // baseUrl :  'https://speclocal.com:4500',
  //main website :-
  //  baseUrl :  'http://192.168.31.121:4500',
  //  baseUrl :  'http://139.59.43.146:4500',
   count_of_unapproved_msg_credit : '/msgcredit/count',
   all_user_unapproved_msg_credit : '/msgcredit/getalldata',
   send_message_credit_to_user : '/msgcredit/updatecredit'
};

