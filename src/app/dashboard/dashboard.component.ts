import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as Chartist from 'chartist';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { MatDialog } from '@angular/material';
import { from } from 'rxjs';
import { SpectviewService } from 'app/spectview.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { RequireMatch as RequireMatch } from '../requireMatch';


import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { AngularCsv } from 'angular7-csv/dist/Angular-csv';
import { ExcelService } from 'app/services/excel.service';



export interface PeriodicElement {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  contactno: number;
  businessname: string;
}

export interface gender {
  value: string;
  viewValue: string;
}

export interface ServiceType {
  name: string;
}

export const ELEMENT_DATA: PeriodicElement[] = [];

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  showUpdateForm: boolean = false;
  showForm: boolean = false;
  showTable: boolean = true;
  showOneUser: boolean = false;
  registrationForm: FormGroup;
  hide = true;
  submitted = false;
  userList: any;
  stateInfo: any[] = [];
  districtInfo: any[] = [];
  resp: any;
  email: any;
  birthday: boolean = false;
  anniversary: boolean = false;
  smscampaining: boolean = false;

  updateUser: {};
  platformname: [];

  id: number;
  disabled = false;
  device: any = [];
  dialogResult = "";
  alluser: any;
  periodicElement: PeriodicElement;
  displayedColumns: string[] = ['sn', 'firstname', 'lastname', 'email', 'messagecredit', 'contactno', 'businessname', 'action'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];

  filteredplatforms: Observable<string[]>;
  platforms: string[] = [];

  serviceTypes: string[] = [];
  filteredServiceTypes: Observable<string[]>;

  allPlatforms: any = [];
  allServiceType: any = [];

  @ViewChild('platformInput') platformInput: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  gender: gender[] = [
    { value: 'male', viewValue: 'Male' },
    { value: 'female', viewValue: 'Female' },
    { value: 'other', viewValue: 'Other' }
  ];



  constructor(public dialog: MatDialog,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private service: SpectviewService,
    private _snackBar: MatSnackBar,
    private _route: Router,
    private fb: FormBuilder,
    private exportAsService: ExportAsService,
    private excelService: ExcelService
  ) {

    this.registration();
    // this.getAllUsers();

    this.getAllRegisteredUsers();
    this.getplatforms();
    this.getServiceType();


    this.iconRegistry.addSvgIcon(
      'envelope',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/email.svg'));
    this.iconRegistry.addSvgIcon(
      'delete',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/rubbish-bin.svg'));
  }
  ngOnInit() {
    this.userList = [];



    // ****************** Mat AutoComplete Filter *************************
    this.filteredServiceTypes = this.registrationForm.get('busuinessservicetype').valueChanges
      .pipe(
        startWith(''),
        map((serviceType: any) => serviceType.length >= 1 ? serviceType ? this._filterServiceType(serviceType) : this.allServiceType.slice() : [])
      );
    this.filteredplatforms = this.registrationForm.get('platformname').valueChanges.pipe(
      startWith(''),
      map((platform: any) => platform.length >= 1 ? platform ? this._filter(platform) : this.allPlatforms.slice() : [])
    )


  }
  // ****************** Add User *************************
  registration() {
    this.registrationForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(4), Validators.pattern('[a-zA-Z]+')]],
      lastname: ['', [Validators.required, Validators.minLength(4), Validators.pattern('[a-zA-Z]+')]],
      dob: ['', Validators.required],
      gender: ['', Validators.required],

      businessname: ['', [Validators.required, Validators.minLength(4)]],
      businessaddress: ['', Validators.required],


      // fbpageurl: ['', [Validators.required, Validators.pattern('(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?')]],
      fbpageurl: ['', [Validators.required, Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]],
      googlepageurl: ['', [Validators.required, Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]],
      busuinessservicetype: ['',[ Validators.required,RequireMatch]],
      noOfEmployees: [''],
      msgcredit: [''],

      contactno: ['', [Validators.required, Validators.pattern(/^(\+\d{1,3}[- ]?)?\d{10}$/)]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required, Validators.minLength(4)]],

      password: ['', [Validators.required, Validators.minLength(6)]],

      platformname: ['', [ Validators.required,RequireMatch]],

      smsserviceid: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^[a-zA-Z ]*$')]],
      smscampaining: [''],
      birthday: [''],
      anniversary: [''],
      role: [''],
    })
  }

  get r() { return this.registrationForm.controls; }

  // ****************** show and hide functions *************************
  showRegistrationForm() {
    this.registrationForm.reset();
    this.showForm = true;
    this.showTable = false;
    this.showUpdateForm = false;
    this.showOneUser = false;
  }
  hideRegistrationForm() {
    this.showForm = false;
    this.showTable = true;
    this.showUpdateForm = false;
    this.showOneUser = false;
  }
  showUpdateUser(email) {
    this.email = email;
    this.getOneRegisteredUser(this.email);
    this.showUpdateForm = true;
    this.showTable = false
    this.showForm = false;
    this.showOneUser = false;
  }
  showUser(email) {
    this.email = email;
    this.getOneRegisteredUser(this.email);
    this.showOneUser = true;
    this.showUpdateForm = false;
    this.showTable = false
    this.showForm = false;
  }
  // ****************** Mat Notifications *************************
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  // ****************** Status Update user *************************
  onChange(value, emailid) {
    if (value.checked === true) {
      let data = {
        email: emailid,
        status: value.checked
      }
      console.log(value.checked);
      this.service.updateUserStatus(data).subscribe(res => {
        console.log("result : ", res);
      });

    } else {
      let data = {
        email: emailid,
        status: value.checked
      }
      console.log(value.checked);
      this.service.updateUserStatus(data).subscribe(res => {
        console.log("result : ", res);
      });
    }

  }
  // table Search Option
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // ****************** convert userList to csv function *************************
  csvOptions = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    showTitle: true,
    title: 'User List :',
    useBom: true,
    noDownload: false,
    headers: ["ID", "First Name", "Last Name", "DOB", "Gender", "Business Name"
      , "Business Service Type", "Business Address",
      "Facebook URL", "Google URL", "Contact No",
      "Email", "Address", "Password", "Platforms", "SMS Sender ID",
      "Birthday", "Anniversary", "SMS Campaining"]
  };

  downloadCSV() {
    new AngularCsv(this.userList, "USERList", this.csvOptions);
  }
  // ************ convert userInfo to excel function **************
  exportToExcel(event) {
    this.excelService.exportAsExcelFile(this.userList, 'users');
  }

  // ****************** Print*************************
  printDiv(divName) {

    var printContents = document.getElementById(divName).innerHTML;
    var w = window.open();
    w.document.write(printContents);
    w.document.write('<scr' + 'ipt type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</sc' + 'ript>');
    w.document.close(); // necessary for IE >= 10
    w.focus(); // necessary for IE >= 10
    return true;

  }


  // ****************** convert userInfo to pdf function *************************
  exportAsConfig: ExportAsConfig = {
    type: 'pdf', // the type you want to download
    elementId: 'viewUserInfo', // the id of html/table element
    options: {
      orientation: "landscape",
      margins: {
        top: '10',
        left: '10',
        right: '10',

      }
    }

  }
  exportAspdf() {
    this.exportAsService.save(this.exportAsConfig, 'user');
    this.exportAsService.get(this.exportAsConfig).subscribe(content => {
      console.log(content);
    });
  }

  // generatePdf() {
  //   this.getOneRegisteredUser(this.email)
  //    console.log('pdf' ,this.email)

  //   var columns = [
  //   { title: "ID",dataKey: "id" },
  //   { title: "Firstname", dataKey: "firstname" },
  //   { title: "Lastname", dataKey: "lastname"},
  //   { title: "DOB" , datakey: "dob"},{ title: "Gender" , datakey: "gender"},
  //   { title: "Business Name" , datakey: "businessname"},{ title: "Business Address" , datakey: "businessaddress"},
  //   { title: "Facebook URL" , datakey: "facebookURL"},{ title: "Google URL" , datakey: "googleURL"},
  //   { title: "Contact No" , datakey: "contactno"},{ title: "Email" , datakey: "email"},
  //   { title: "Address" , datakey: "address"},{ title: "Password" , datakey: "password"},
  //   { title: "Platforms" , dataKey: "platforms"}, { title: "SMS Sender ID" , dataKey: "smssenderid"}, 
  //   { title: "Birthday" , dataKey: "birthday"}, { title: "Anniversary" , dataKey: "anniversary"},
  //   { title: "SMS Campaining" , dataKey: "smscampaining"},
  //   ];

  //   var rows = this.updateUser
  //   var doc = new jsPDF('p', 'pt');

  //   var header = function (data) {
  //     doc.setFontSize(18);
  //     doc.setTextColor(40);
  //     doc.setFontStyle('normal');
  //     doc.text("User Details", data.settings.margin.left, 50);
  //   };

  //   doc.autoTable(columns, rows, { margin: { top: 80 }, beforePageContent: header });
  //   doc.save("table.pdf");
  // }



  // ****************** Mat AutoComplete Filter *************************
  private _filterServiceType(type: string): string[] {
    const filtertype = type.toString().toLowerCase();
    return this.allServiceType.filter(serviceTypes => serviceTypes.businessServiceType.toString().toLowerCase().indexOf(filtertype) === 0);
  }
  private _filter(platformsvalue: string): any[] {
    const filterPlatformValue = platformsvalue.toString().toLowerCase();
    return this.allPlatforms.filter(platform => platform.platformName.toString().toLowerCase().indexOf(filterPlatformValue) === 0);
  }

  // ****************** Mat Chips Functions *************************
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our platform
    if ((value || '').trim()) {
      if (this.platforms.includes(value) === false) this.platforms.push(value.trim());
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
    this.registrationForm.get('platformname').setValue('null')
  }
  remove(platform: string): void {
    const index = this.platforms.indexOf(platform);
    if (index >= 0) {
      this.platforms.splice(index, 1);
    }
  }
  selected(event: MatAutocompleteSelectedEvent): void {
    this.platforms.push(event.option.viewValue);
    this.platformInput.nativeElement.value = '';
    this.registrationForm.get('platformname').setValue('null')
  }

  displayWith(obj?: any): string | undefined {
    return obj ? obj.name : undefined;
  }
  // ****************** OnSubmit register Form *************************
  registerUser() {
    this.registrationForm.patchValue({
      platformname: this.platforms,
      role: 'user'
    })
    this.service.addUser(this.registrationForm.value).subscribe(res => {
      if (res['success']) {
        this.openSnackBar('user added succefully', '');
        this.service.getAllRegisteredUsers().subscribe((res) => { });
        this._route.navigate(['/dashboard'])
        this.showForm = false;
        this.showTable = true;
      } else if (res['msg'] == 'user already exists') {
        this.openSnackBar('user already exits', '')
        this.showForm = false;
        this.showTable = true;
      } else {
        this.openSnackBar('err', '')
        this.showForm = false;
        this.showTable = true;
      }
    })
  }
  // ****************** get User *************************
  getAllRegisteredUsers() {
    this.service.getAllRegisteredUsers().subscribe(res => {
      console.log("result get all users :", res);
      this.alluser = res['data']
      this.userList = this.alluser;
      if (res['success']) {
        // this.dataSource = res['data'];
        this.dataSource = new MatTableDataSource(this.alluser)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }

    })
  }
  // ****************** update User *************************
  updateRegisteredUsers() {
    this.service.updateRegistereUser(this.registrationForm.value).subscribe(res => {
      console.log('updated data', res)
      if (res['success']) {
        this.getAllRegisteredUsers();
        this.openSnackBar('updated successfully', '')
        this.showUpdateForm = false;
        this.showTable = true;
      }

    })
  }
  // ****************** get Platform **********************
  getplatforms() {
    this.service.getPlatforms().subscribe(res => {
      if (res['success']) {
        this.allPlatforms = res['data'];
      }
    })
  }

  // ****************** get Service type **********************
  getServiceType() {
    this.service.getBusinessServiceType().subscribe(res => {
      if (res['success']) {
        this.allServiceType = res['data']
      }
    })
  }
  // ****************** get user by email **********************
  getOneRegisteredUser(email: any) {
    this.service.getOneRegisteredUser(email).subscribe(res => {
      if (res['success']) {
        this.updateUser = res['data']
        this.platformname = res['data.platformname']
      }
    })
  }

}
