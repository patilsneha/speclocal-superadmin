import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})


export class ChangePasswordComponent implements OnInit {
  hide = true;
  
  changePasswordForm : FormGroup = this.fb.group({
    oldPassword : ['',Validators.required],
    newPassword : ['',Validators.required],
    confirmPassword: ['', Validators.required]
  })

  constructor(public thisDialogRef: MatDialogRef<ChangePasswordComponent>,@Inject(MAT_DIALOG_DATA) public data: string , private fb : FormBuilder) { }

  ngOnInit() {
  }

  onSubmit(){
    console.log(this.changePasswordForm.value)
  }

}
