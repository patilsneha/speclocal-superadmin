import { Component, OnInit , Inject, ViewChild} from '@angular/core';
import { MatDialogRef, MatInput, MatSnackBar, } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SpectviewService } from 'app/spectview.service';

import { Router } from '@angular/router';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  editUserForm : FormGroup;

  @ViewChild('ip') nameInput: MatInput;

  constructor(
    public thisDialogRef: MatDialogRef<EditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
     private service :SpectviewService, 
     private _route :Router,
    private _snackBar: MatSnackBar ) { 

      this.editUserForm = new FormGroup({
        firstname : new FormControl('',[Validators.required,Validators.minLength(4), Validators.pattern('[a-zA-Z]+')]),
        lastname : new FormControl('',[Validators.required,Validators.minLength(4),Validators.pattern('[a-zA-Z]+')]),
        email : new FormControl('',[Validators.required,Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]),
        contactno : new FormControl('',[Validators.required,Validators.pattern(/^(\+\d{1,3}[- ]?)?\d{10}$/)]),
        businessname : new FormControl('',[Validators.required,Validators.minLength(4)])
      })
    }
    editUser(){
      this.service.editUser(this.editUserForm.value).subscribe((res :any)=>{
        console.log(res) 
        if(res['success']){
          this.openSnackBar('User updated successfully','');
          this.thisDialogRef.close("close");
          this.service.getAllUsers().subscribe((res :any)=>{});
          this._route.navigate(['/dashboard']);
        }
      });
    }
    openSnackBar(message: string, action: string) {
      this._snackBar.open(message, action, {
        duration: 2000,
      });
    }
  ngOnInit() {
  }

}
