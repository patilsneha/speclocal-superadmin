import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiresViewComponent } from './enquires-view.component';

describe('EnquiresViewComponent', () => {
  let component: EnquiresViewComponent;
  let fixture: ComponentFixture<EnquiresViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiresViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiresViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
