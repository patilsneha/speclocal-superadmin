import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators,FormBuilder } from '@angular/forms';
import { formControlBinding } from '@angular/forms/src/directives/ng_model';
@Component({
  selector: 'app-enquires-view',
  templateUrl: './enquires-view.component.html',
  styleUrls: ['./enquires-view.component.scss']
})
export class EnquiresViewComponent implements OnInit {

  enquiryFrom : FormGroup = this.fb.group({
    fullName : ['',Validators.required],
    email : ['',Validators.required],
    contactNo : ['',Validators.required],
    course : ['',Validators.required],
    message : ['',Validators.required] 
  })
  constructor(public thisDialogRef: MatDialogRef<EnquiresViewComponent>,@Inject(MAT_DIALOG_DATA) public data: string , private fb: FormBuilder ) { }
  

  ngOnInit() {
  }

  onSubmit(){
    console.log(this.enquiryFrom.value)
  }
}
