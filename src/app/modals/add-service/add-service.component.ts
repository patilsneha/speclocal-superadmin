import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MatInput, MatSnackBar, } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { SpectviewService } from 'app/spectview.service';

@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.scss']
})
export class AddServiceComponent implements OnInit {

  addServiceForm :FormGroup;
  submitted = false;
  serviceType: string[] = ["type-1", "type-2", "type-3"];
  filteredServiceTypes: Observable<string[]>;

  constructor(
    public dialogRef: MatDialogRef<AddServiceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _route: Router,
    private _snackBar: MatSnackBar,
    private service : SpectviewService) { }

  ngOnInit() {
    this.addService();
     // filter for autocomplete 
     this.filteredServiceTypes = this.addServiceForm.get('businessServiceType').valueChanges
     .pipe(
       startWith(''),
       map(value => value.length >= 1 ? this._filterServiceType(value) : [])
     );
  }
  private _filterServiceType(type: string): string[] {
    const filtertype = type.toLowerCase();
    return this.serviceType.filter(serviceType => serviceType.toLowerCase().includes(filtertype));
  }

  addService(){
    this.addServiceForm = new FormGroup({
      businessServiceType : new FormControl('',[Validators.required,Validators.minLength(4), Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]),
            
    })
  }
  onSubmitService(){
    console.log(this.addServiceForm.value)
    // this.service.registerUser(this.registrationForm.value).subscribe(res => {
      this.service.addBusinessServiceType(this.addServiceForm.value).subscribe(res =>{
        console.log("service response : "  ,res);

        if(res['success']){
          
          this.openSnackBar('Service type added Succefully','')
          this.dialogRef.close()
        }else if(res['msg'] == 'service type already exits'){
          this.openSnackBar('Service type already exist','')
          this.dialogRef.close()
        }else{
          this.openSnackBar('err','')
          this.dialogRef.close()
        }
      })

  }
  get p(){
    return this.addServiceForm.controls
  }
  openSnackBar(message : string ,action : string){
    this._snackBar.open(message , action , {
      duration : 2000
    })
  }
}
