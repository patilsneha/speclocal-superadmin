import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-log-out',
  templateUrl: './log-out.component.html',
  styleUrls: ['./log-out.component.scss']
})
export class LogOutComponent implements OnInit {

  constructor(public thisDialogRef: MatDialogRef<LogOutComponent>,@Inject(MAT_DIALOG_DATA) public data: string, private _router : Router) { }

  ngOnInit() {
  }
  closeDialog(){
    this.thisDialogRef.close();
  }
  logout(){
    localStorage.clear();
    this._router.navigate(['/login'])
  }
}
