import { Component, OnInit, Inject } from '@angular/core';
import { SpectviewService } from 'app/spectview.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-delete-service',
  templateUrl: './delete-service.component.html',
  styleUrls: ['./delete-service.component.scss']
})
export class DeleteServiceComponent implements OnInit {

  constructor(public thisDialogRef: MatDialogRef<DeleteServiceComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data: string,
    private service : SpectviewService,
    private _snackBar : MatSnackBar) { }

  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  deleteBusinessServiceType(){
    this.service.deleteBusinessServiceType(this.data).subscribe(res => {
      console.log(res)
      if(res['success']){
        this.openSnackBar('deleted service type', '');
      }
    })
  }
  closeDialog(){
    this.thisDialogRef.close()
  }

}
