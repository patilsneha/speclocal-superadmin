import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SpectviewService } from 'app/spectview.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deletemodal',
  templateUrl: './deletemodal.component.html',
  styleUrls: ['./deletemodal.component.scss']
})
export class DeletemodalComponent implements OnInit {

  constructor(public thisDialogRef: MatDialogRef<DeletemodalComponent>,
              @Inject(MAT_DIALOG_DATA) 
              public data: string,
              private service : SpectviewService,
              private _route : Router,
              private _snackBar : MatSnackBar) { }

  ngOnInit() {
  
  }
 
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  deletePlatform(){
    this.service.deletePlatforms(this.data).subscribe(res => {
      console.log('deleted' , res)
      if(res['success']){
        this.openSnackBar('platform deleted ', '');
      }
    })
  }
  closeDialog(){
    this.thisDialogRef.close()
  }

}
