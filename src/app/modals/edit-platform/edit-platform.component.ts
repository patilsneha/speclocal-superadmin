import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MatInput, MatSnackBar, } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { SpectviewService } from 'app/spectview.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { RequireMatch as RequireMatch } from '../../../app/requireMatch';

@Component({
  selector: 'app-edit-platform',
  templateUrl: './edit-platform.component.html',
  styleUrls: ['./edit-platform.component.scss']
})
export class EditPlatformComponent implements OnInit {

  editPlatformForm: FormGroup;
  serviceTypes: string[] = [];
  filteredServiceTypes: Observable<string[]>;

  allPlatforms: any = [
  ];


  constructor(
    public thisDialogRef: MatDialogRef<EditPlatformComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _route: Router,
    private _snackBar: MatSnackBar,
    private service: SpectviewService
  ) { }

  ngOnInit() {
    this.editPlatform();
    // filter for autocomplete 
    this.filteredServiceTypes = this.editPlatformForm.get('businessServiceType').valueChanges
      .pipe(
        startWith(''),
        map((serviceType: any) => serviceType.length >= 1 ? serviceType ? this._filterServiceType(serviceType) : this.allPlatforms.slice() : [])
      );
  }

  private _filterServiceType(type: string): string[] {
    const filtertype = type.toLowerCase();
    return this.allPlatforms.filter(serviceTypes => serviceTypes.businessServiceType.toString().toLowerCase().indexOf(filtertype) === 0);
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  editPlatform() {
    this.editPlatformForm = new FormGroup({
      businessServiceType: new FormControl('', [Validators.required, RequireMatch, Validators.minLength(4)]),
      platformName: new FormControl('', [Validators.required, Validators.minLength(4), Validators.minLength(4)]),
      // platformLogo: new FormControl('', [Validators.required, Validators.minLength(4)]),
      platformLink: new FormControl('', [Validators.required, Validators.minLength(4)]),
      id: new FormControl('')
    })
  }
  onUpdatePlatform() {
    console.log(this.editPlatformForm.value)
    this.editPlatformForm.patchValue({
      id: this.data._id
    })
    this.service.updatePlatforms(this.editPlatformForm.value).subscribe((res) => {
      console.log(res)
      if (res['success']) {
        this.openSnackBar('updated successfully', '')
        this.thisDialogRef.close()
      }
    })
  }
  get p() {
    return this.editPlatformForm.controls;
  }
  getServiceType() {
    this.service.getBusinessServiceType().subscribe(res => {
      this.allPlatforms = res['data']
      // if(['success']){
      //   this.allPlatforms = res['data']
      // }
  
    })
  }
  displayWith(obj?: any): string | undefined {
    return obj ? obj.name : undefined;
  }
}
