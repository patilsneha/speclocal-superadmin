import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MatInput, MatSnackBar, } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { SpectviewService } from 'app/spectview.service';
import { FileUploader } from 'ng2-file-upload';
import { RequireMatch as RequireMatch } from '../../../app/requireMatch';

@Component({
  selector: 'app-add-platform',
  templateUrl: './add-platform.component.html',
  styleUrls: ['./add-platform.component.scss']
})
export class AddPlatformComponent implements OnInit {
  businessServicetypes :any;
  addPlatformForm: FormGroup;
  resp: any;
  submitted = false;

  @ViewChild('imageSelect') imageFile: ElementRef;
  image = null;
  hide = true;
  public uploader: FileUploader = new FileUploader({ url: "http://localhost:5500/api/uploadimage", itemAlias: 'image' });
  
  serviceTypes: string[] = [];
  filteredServiceTypes: Observable<string[]>;

  allPlatforms: any = [
  ];
  
  constructor(
    public dialogRef: MatDialogRef<AddPlatformComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _route: Router,
    private _snackBar: MatSnackBar,
    private service: SpectviewService,
    ) {
    this.getServiceType();
  }

  ngOnInit() {
    this.addPlatform();

    // filter for autocomplete 
    this.filteredServiceTypes = this.addPlatformForm.get('businessServiceType').valueChanges
      .pipe(
        startWith(''),
        map((serviceType : any) => serviceType.length >= 1 ? serviceType ? this._filterServiceType(serviceType) : this.allPlatforms.slice(): [])
      );

  }

  private _filterServiceType(type: string): string[] {
    const filtertype = type.toLowerCase();
    return this.allPlatforms.filter(serviceTypes => serviceTypes.businessServiceType.toString().toLowerCase().indexOf(filtertype) === 0);
  }

  addPlatform() {
    this.addPlatformForm = new FormGroup({
      businessServiceType: new FormControl('', [Validators.required, Validators.minLength(4),RequireMatch, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]),
      platformName: new FormControl('', [Validators.required, Validators.minLength(4), Validators.minLength(4), Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]),
      platformLogo : new FormControl('',[Validators.required]),
      // platformLogo: new FormControl('', [Validators.required, Validators.minLength(4), Validators.pattern('^https?://(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:/[^/#?]+)+\.(?:jpg|gif|png)$')]),
      platformLink: new FormControl('', [Validators.required, Validators.minLength(4), Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')])

    })
  
  }

  get p() {
    return this.addPlatformForm.controls;
  }
  
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  onSubmitPlatform() {
    this.service.addPlatforms(this.addPlatformForm.value).subscribe(res=>{
      console.log("new Platform : " , res);
     
      if (res['success']) {
        this.openSnackBar('platform added succefully', '');
       this.dialogRef.close()
      } else if(res['msg'] == 'platform already exists'){
        this.openSnackBar('platform already exist','')
        this.dialogRef.close()
      }else{
        this.openSnackBar('err','')
        this.dialogRef.close()
      }
    })
    
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false };
    this.uploader.onCompleteItem = (item: any, res: any, status: any, headers: any) => {
      this.image = res;
      console.log('image' , this.image)
      this.addPlatformForm.get('platformLogo').setValue(res);
    }
  }

  getServiceType() {
    this.service.getBusinessServiceType().subscribe(res => {
      this.allPlatforms = res['data']
      console.log(this.allPlatforms)
    })
  }

  displayWith(obj?: any): string | undefined {
    return obj ? obj.name : undefined;
  }

  // addplatforms(){
  //   this.service.addPlatforms(this.addPlatformForm.value).subscribe(res => {
  //   })
  // }

}