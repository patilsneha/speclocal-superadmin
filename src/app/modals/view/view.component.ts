import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageService } from 'app/services/message.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  selectedUser: any;
  charge: number = 100;
  formdata: any = {};
  constructor(public thisDialogRef: MatDialogRef<ViewComponent>,
     @Inject(MAT_DIALOG_DATA) 
     public data: string, 
     private _msgService: MessageService) {
    this.selectedUser = data;
    this.formdata.email = this.selectedUser.email;
    this.formdata.msgcredit = this.selectedUser.msgcredit;
  }
  ngOnInit() {
  }  
  save() {
    this.formdata._id = this.selectedUser._id;
    this._msgService.sendMessageCreditToUser(this.formdata).subscribe((res: any) => {
      if (res.success) {
        this.thisDialogRef.close(res.success);
      }
    });
  }


}
