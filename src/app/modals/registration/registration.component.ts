import { Component, OnInit , Inject, ViewChild} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MatInput } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { SpectviewService } from 'app/spectview.service';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import { StateService } from '../../../app/services/state.service'


export interface gender {
  value : string ;
  viewValue : string ;
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  registrationForm : FormGroup;
  resp : any;
  close :boolean = false
  hide : boolean= true;

  stateInfo: any[] = [];
  districtInfo: any[] = [];

  // mat accordian steps
  step = 0;

    setStep(index: number) {
      this.step = index;
    }
  
    nextStep() {
      this.step++;
    }
  
    prevStep() {
      this.step--;
    }

    // select Gender
    gender: gender[] = [
      {value: 'male-0', viewValue: 'Male'},
      {value: 'female-1', viewValue: 'Female'},
      {value: 'other-2', viewValue: 'Other'}
    ];

  @ViewChild('ip') nameInput: MatInput;

  constructor( 
    public thisDialogRef: MatDialogRef<RegistrationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service :SpectviewService, 
    private _route :Router,
    private _snackBar: MatSnackBar,
    private fb : FormBuilder,
    private stateService : StateService ) { 

    this.registrationForm = new FormGroup({
      firstname : new FormControl('',[Validators.required,Validators.minLength(4), Validators.pattern('[a-zA-Z]+')]),
      lastname : new FormControl('',[Validators.required,Validators.minLength(4),Validators.pattern('[a-zA-Z]+')]),
      email : new FormControl('',[Validators.required,Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]),
      contactno : new FormControl('',[Validators.required,Validators.pattern(/^(\+\d{1,3}[- ]?)?\d{10}$/)]),
      businessname : new FormControl('',[Validators.required,Validators.minLength(4)]),
      password : new FormControl('',[Validators.required]),
      role :new FormControl('')
    })

    
  }
  ngOnInit() {
    this.getStateData();
  }

  
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


  registerUser(){
    this.registrationForm.patchValue({
      role : 'user'
    });
    console.log(this.registrationForm.value);
    this.service.registerUser(this.registrationForm.value).subscribe(res =>{
      console.log("result:",res);
      this.resp = res;
      if(res['success']){
        this.openSnackBar('User added successfully','');
        this.thisDialogRef.close("close");
        this.service.getAllUsers().subscribe((res :any)=>{});
        this._route.navigate(['/dashboard']);
      }else{
        if (this.resp.msg!='err') {
          this.openSnackBar('User Already Exists','');
          this.thisDialogRef.close("close");
        } else {
          this.openSnackBar('Error','');
          this.thisDialogRef.close("close");
        }
      }
    });
  }

  // state and district
  getStateData(){
    this.stateService.getAllStateData().subscribe(

      data => {
        console.log(data.states);
        this.stateInfo=data.states;
      },
    ) 
    // this.districtInfo=this.stateInfo[0].districts;
  } 
  onChange(obj){
    var district  = this.stateInfo.filter(res=>{
      return res.state === obj.source.value
    });
    console.log('district :',district[0].districts);
    this.districtInfo = district[0].districts;
  } 
}
