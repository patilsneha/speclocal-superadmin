import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SpectviewService } from 'app/spectview.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-service-type',
  templateUrl: './edit-service-type.component.html',
  styleUrls: ['./edit-service-type.component.scss']
})
export class EditServiceTypeComponent implements OnInit {

  updateServiceForm :FormGroup;
  constructor(private service : SpectviewService,
    public thisDialogRef: MatDialogRef<EditServiceTypeComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data: any,
    private _route : Router,
    private _snackBar : MatSnackBar) { }

  ngOnInit() {
    this.updateService()
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  updateService(){
    this.updateServiceForm = new FormGroup({
      businessServiceType : new FormControl('',[Validators.required,Validators.minLength(4)]),
      id : new FormControl('')  
    })
  }

  onUpdateService(){
    this.updateServiceForm.patchValue({
       id : this.data._id
    })
    this.service.updateBusinessServiceType(this.updateServiceForm.value).subscribe((res) => {
      if(res['success']){
        this.openSnackBar('updated successfully','')
        this.thisDialogRef.close()
      }
    })
    
  }
}
