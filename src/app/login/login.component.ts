import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../guards/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { formControlBinding } from '@angular/forms/src/directives/ng_model';
// import {MatSnackBar} from '@angular/material/snack-bar';
import { from } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted: boolean = false;
  hide = true;
  constructor(private _authService: AuthService, private _router: Router,
    private _snackBar: MatSnackBar
    ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }
  get f() {
    return this.loginForm.controls;
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this._authService.loginCurrentUser((this.loginForm.value.email).trim(), (this.loginForm.value.password).trim());
    if (this.submitted == true) {
      this.openSnackBar('User Logged In successfully','');
      this._router.navigate(['/dashboard']);
    }
  }
  login(){
    this._authService.login((this.loginForm.value.email).trim() ,(this.loginForm.value.password).trim())
    .subscribe(user=>{
      if (user['success'] === true) {
        this.openSnackBar('User Logged In successfully','');
        this._router.navigate(['/dashboard']);
      } else {
        this.openSnackBar('Invalid','');
      }
    })
  }
}
