import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MessageService {
  requestCount: number = 0;
  constructor(public _http: HttpClient) {

  }
  // getCountRequestForCreditMessage() : any{
  //    this._http.get(environment.baseUrl + environment.count_of_unapproved_msg_credit).subscribe((res: any) => {
  //     console.log(res.data[0]._id)
  //     if (!res.data[0]._id) {
  //       this.requestCount = res.data[0].status
  //       return  this.requestCount;
  //     }
  //   });
  // }
  getAllRequestForCreditMessage(){
    return this._http.get(environment.baseUrl + environment.all_user_unapproved_msg_credit);
  }
  sendMessageCreditToUser(data : any){
    return this._http.put(environment.baseUrl + environment.send_message_credit_to_user,data);
  }
}
