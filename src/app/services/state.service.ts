import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  path: string = "../../assets/json/state.json";
 
  AllState: string;
  constructor(private http : HttpClient) {
   }
  getAllStateData(): Observable<any>{ 
    return this.http.get(this.path)
  }      
}
