import { Component, OnInit , ViewChild} from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddServiceComponent } from 'app/modals/add-service/add-service.component';
import { SpectviewService } from 'app/spectview.service';
import { Router } from '@angular/router';
import { DeleteServiceComponent } from 'app/modals/delete-service/delete-service.component';
import { EditServiceTypeComponent } from 'app/modals/edit-service-type/edit-service-type.component';


export interface PeriodicElement {
  id: number;
  businessservicename : string,
}

export const ELEMENT_DATA: PeriodicElement[] = [];

@Component({
  selector: 'app-service-type',
  templateUrl: './service-type.component.html',
  styleUrls: ['./service-type.component.scss']
})
export class ServiceTypeComponent implements OnInit {

  periodicElement: PeriodicElement;
  displayedColumns: string[] = ['sn','businessservicename', 'action'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  servicetypes : any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,
    private iconRegistry: MatIconRegistry,
    private _snackBar: MatSnackBar,
    private service : SpectviewService ,
    private _routes : Router) { 
      this.getServiceType();
    }

  ngOnInit() {
  }

  openaddServiceModal(){
    let dialogRef = this.dialog.open(AddServiceComponent,{
      panelClass: 'full-screen-modal',
      width: '500px',
    });
  }
  
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  openDeleteDialog(data){
    let dialogRef = this.dialog.open(DeleteServiceComponent,{
      panelClass: 'full-screen-modal',
      width: '500px',
      data : data
    })
  }
  openUpdateDialog(data){
    console.log("vaisjhali : " ,data)
    let dialogRef = this.dialog.open(EditServiceTypeComponent, {
      panelClass: 'full-screen-modal',
      width: '500px',
      data : data
    })
  }
  // tableSearchOption
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getServiceType(){
    this.service.getBusinessServiceType().subscribe(res=>{
      console.log(res)
      this.servicetypes = res['data']
      this.dataSource = new MatTableDataSource(this.servicetypes)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }
}
