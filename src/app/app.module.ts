import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { AppComponent } from './app.component';
import { NgxStateModule } from 'ngx-state';
import { ExportAsModule } from 'ngx-export-as';
import { NgxUiLoaderModule, NgxUiLoaderRouterModule } from  'ngx-ui-loader';
import { AgmCoreModule } from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';

import 
{ 
  MatCardModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatRippleModule,
  MatInputModule,
  MatDialogModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatMenuModule,
  MatIconModule,
  MatSnackBarModule,
  
  MatAutocompleteModule,
 } from '@angular/material';
import { ChangePasswordComponent } from './modals/change-password/change-password.component';
import { LogOutComponent } from './modals/log-out/log-out.component';
import { ViewComponent } from './modals/view/view.component';
import { MaterialFileInputModule } from 'ngx-material-file-input';

@NgModule({
  imports: [
    BrowserAnimationsModule,    
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatIconModule,
    MatFormFieldModule,
    MaterialFileInputModule,
    MatInputModule,
    MatRippleModule,
    MatDialogModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatMenuModule,
    NgxUiLoaderModule, 
    NgxUiLoaderRouterModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    ExportAsModule ,

    NgxStateModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_GOOGLE_MAPS_API_KEY'
    }),
    HttpClientModule,
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    ChangePasswordComponent,
    LogOutComponent,
    ViewComponent,
    

  ],
  entryComponents:[
    ChangePasswordComponent,
    LogOutComponent,
    ViewComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
