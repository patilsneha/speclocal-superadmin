import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  redirectUrl: string;
  baseUrl: string = environment.baseUrl;
  currentUser: User = { email: '', password: ''};
  constructor(public http: HttpClient) { }

  loginCurrentUser(usr_name: string, usr_password: string){
    this.currentUser = {email: usr_name, password: usr_password}
    console.log(this.currentUser);
    if (this.currentUser.email == 'admin' && this.currentUser.password == 'admin') {
      localStorage.setItem('currentuser',JSON.stringify(this.currentUser));
    }
  }
  login(email: any, password: any){
    return this.http.post(this.baseUrl + '/auth/login', { email: email, password: password })
  }
}
