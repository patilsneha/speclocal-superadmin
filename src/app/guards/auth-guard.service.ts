import { Injectable } from '@angular/core';
import { AuthService } from '../guards/auth.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Route } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  

  constructor(private _authService: AuthService, private _router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if(localStorage.getItem('currentuser')) {
      return true;
    }
      this._router.navigate(['/login'] , { queryParams: { returnUrl: state.url } });
    return false;
  }

  // constructor(private _authService: AuthService, private _router: Router) { }
  // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
  //   if(this._authService.isAuthnticated()) {
  //     return true;
  //   }
  //     this._router.navigate(['/login'] , { queryParams: { returnUrl: state.url } });
  //   return false;
  // }
}
