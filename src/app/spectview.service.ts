import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpectviewService {
  // baseUrl: string = "http://localhost:4500/";
  baseUrl = environment.baseUrl;
  getHeaders() {
    var headers_object = new HttpHeaders();
    // headers_object.append('Content-Type', 'application/json');
    headers_object.append("Authorization", "Basic " + localStorage.getItem('token'));
    const httpOptions = {
      headers: headers_object
    };

    return httpOptions;
  }

  constructor(private _httpClient: HttpClient) { }
  loginUser(data: any) {
    return this._httpClient.post(this.baseUrl + '/auth/login', data);
  }
  registerUser(data: any) {
    return this._httpClient.post(this.baseUrl + '/auth/register', data);
  }
  getAllUsers() {
    return this._httpClient.get(this.baseUrl + '/user/users', this.getHeaders());
  }
  editUser(udata){
    return this._httpClient.put(this.baseUrl + '/user/updateuser', udata);
  }
  getuserByEmail(email :any){
    return this._httpClient.get(this.baseUrl + '/user/users/'+ email);
  }
  updateUserStatus(data: any) {
    return this._httpClient.put(this.baseUrl + '/user/changeStatus', data);
  }
  
  getAllRequestedContacts() {
    return this._httpClient.get(this.baseUrl + '/enquiry/getallenquiry', this.getHeaders());
  }



  //new functions
  addBusinessServiceType(data : any){
    return this._httpClient.post(this.baseUrl + '/servicetype/addservicetype', data)
  }
  getBusinessServiceType(){
    return this._httpClient.get(this.baseUrl + '/servicetype/getservicetype')
  }
  deleteBusinessServiceType(serviceType : any){
    return this._httpClient.delete(this.baseUrl + '/servicetype/deleteservicetype/' + serviceType)
  }
  updateBusinessServiceType(data : any){
    return this._httpClient.put(this.baseUrl + '/servicetype/updateservicetype' , data)
  }

  addPlatforms(data : any){
    return this._httpClient.post(this.baseUrl + '/platform/addplatform',data)
  }
  getPlatforms(){
    return this._httpClient.get(this.baseUrl + '/platform/getplatform');
  }
  deletePlatforms(platform :any){
    return this._httpClient.delete(this.baseUrl + '/platform/deleteplatform/' + platform)
  }

  updatePlatforms(data : any){
    return this._httpClient.put(this.baseUrl + '/platform/updateplatform' , data)
  }

  addUser(data : any){
    console.log("in add user service : ",data)
    return this._httpClient.post(this.baseUrl + '/auth/register' ,data);
  }
  getAllRegisteredUsers(){
    return this._httpClient.get(this.baseUrl + '/user/users');
  }
  updateRegistereUser(data : any){
    return this._httpClient.put(this.baseUrl + '/user/updateuser' , data)
  }
  getOneRegisteredUser(email : string){
    return this._httpClient.get(this.baseUrl + '/user/user/' + email)
  }

}
