import { Component, OnInit, ViewChild } from '@angular/core';
import * as Chartist from 'chartist';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry, MatSnackBar, MatDialog } from '@angular/material';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SpectviewService } from 'app/spectview.service';
import { MessageService } from 'app/services/message.service';
import { ViewComponent } from 'app/modals/view/view.component';


export interface PeriodicElement {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  contactno: number;
  businessname: string;
}

export const ELEMENT_DATA: PeriodicElement[] = [];


@Component({
  selector: 'app-sms-services',
  templateUrl: './sms-services.component.html',
  styleUrls: ['./sms-services.component.scss']
})
export class SmsServicesComponent implements OnInit {
  id: number;
  periodicElement: PeriodicElement;
  disabled = false;
  device: any = [];
  dialogResult = "";
  alluser: any;
  displayedColumns: string[] = ['sn', 'user', 'email', 'SMS_Services', 'contactno', 'action'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public dialog: MatDialog, 
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer, 
    private service: SpectviewService,
    private _snackBar: MatSnackBar,
    private _msgService : MessageService) {
      this.iconRegistry.addSvgIcon(
        'envelope',
        this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/email.svg'));
     }


  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  // getAllcreditRequest() {
  //   this._msgService.getAllRequestForCreditMessage().subscribe((res: any) => {
  //     console.log(res);
  //     if (res.success) {
  //       this.alluser = res.data.filter(result => {
  //         return result.status == false;
  //       });
  //       this.dataSource = new MatTableDataSource(this.alluser)
  //       this.dataSource.paginator = this.paginator;
  //       this.dataSource.sort = this.sort;
  //     }
  //   })
  // }
  // tableSearchOption
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  openViewDialog(data: any) {
    let dialogRef = this.dialog.open(ViewComponent, {
      // maxWidth: '100vw',
      // maxHeight: '100vh',
      // height: '100%',
      // width: '100%',
      data: data
      // position:{
      //   right: '10px'
      // },
      // height: '100%',
      // width: '100vw',
      // panelClass: 'full-screen-modal',
    }
    );
    dialogRef.afterClosed().subscribe(result => {
      // console.log('dialog closed: ${result}');
      this.dialogResult = result;
      console.log(this.dialogResult);
      if (this.dialogResult) {
        // this.getAllcreditRequest();
      }
    })
  }


}
