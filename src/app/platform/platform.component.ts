import { Component, OnInit ,ViewChild} from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddPlatformComponent } from '../modals/add-platform/add-platform.component'
import { SpectviewService } from 'app/spectview.service';
import { Observable } from 'rxjs';
import { DeletemodalComponent } from 'app/modals/deletemodal/deletemodal.component';
import { EditPlatformComponent } from 'app/modals/edit-platform/edit-platform.component';


export interface PeriodicElement {
  id: number;
  businessservicetype: string;
  platformname: string;
  platformlogo: string;
  platformlink: number;
  
}

export const ELEMENT_DATA: PeriodicElement[] = [];

@Component({
  selector: 'app-platform',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.scss']
})
export class PlatformComponent implements OnInit {

  platforms : any;
  periodicElement: PeriodicElement;
  displayedColumns: string[] = ['sn', 'businessservicetype', 'platformname',   'action'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dialogResult = "";
  businessServicetypes :any
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,
    private iconRegistry: MatIconRegistry,
    private _snackBar: MatSnackBar,private service : SpectviewService) { 
      this.getplatforms();
    }

  ngOnInit() {
  }

  openAddPlatformModal(){
    let dialogRef = this.dialog.open(AddPlatformComponent,{
      panelClass: 'full-screen-modal',
      width: '500px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('dialog closed: ${result}');
      this.dialogResult = result;
      
    })
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  // tableSearchOption
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

 

getplatforms(){
  this.service.getPlatforms().subscribe(res=>{
    console.log(res);
    this.platforms  = res['data'];
    this.dataSource = new MatTableDataSource(this.platforms)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
  })
}

openDeleteDialog(platformName) {
  console.log(platformName)
  let dialogRef = this.dialog.open(DeletemodalComponent, {
    width: '390px',
    panelClass: 'confirm-dialog-container',
    disableClose: true,
    data: platformName
  }
  );
  dialogRef.afterClosed().subscribe(res => {
    this.dialogResult = res;
  })
}

openEditPlatformDialog(data) {
  console.log("platform Data : ", data)
  let dialogRef = this.dialog.open(EditPlatformComponent, {
    data: data,
    width: '500px',
    panelClass: 'full-screen-modal'
  });
  // dialogRef.afterClosed().subscribe(result => {
  //   console.log('dialog closed : ${result}');
  //   this.dialogResult = result;
  // })
}

}
