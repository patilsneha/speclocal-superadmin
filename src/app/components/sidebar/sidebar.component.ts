import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/message.service';
// getCountRequestForCreditMessage
declare const $: any;
var requestCount =0;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    count ?: number;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
  { path: '/requestedContacts', title: 'Enquiry List', icon: 'person_add', class: ''},
  { path: '/addcredit', title: 'Message Credit', icon: 'chat', class: '',count : requestCount},
  { path: '/servicetype' , title: 'Service Types' , icon: 'work' ,class: ''} ,
  { path: '/platform' , title: 'Platform' , icon: 'card_membership' ,class: ''},
  { path: '/smsServices' , title : 'SMS Services' , icon: 'email' ,class: ""}
  
  
  // { path: '/admission', title: 'Admission', icon: 'person_add', class: ''},
  // // { path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
  // { path: '/table-list', title: 'Table List',  icon:'content_paste', class: '' },
  // { path: '/icons', title: 'Icons',  icon:'bubble_chart', class: '' },
  // { path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  
  constructor(private _messageService : MessageService) { 
    // this.getRequestCount();
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  // getRequestCount(){
  //   requestCount = this._messageService.getCountRequestForCreditMessage();
  //   console.log(requestCount)
  // }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
