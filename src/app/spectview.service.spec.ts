import { TestBed, inject } from '@angular/core/testing';

import { SpectviewService } from './spectview.service';

describe('SpectviewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpectviewService]
    });
  });

  it('should be created', inject([SpectviewService], (service: SpectviewService) => {
    expect(service).toBeTruthy();
  }));
});
