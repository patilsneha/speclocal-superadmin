import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AdminLayoutRoutes } from './admin-layout.routing';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { PlatformComponent } from '../../platform/platform.component';
import { ServiceTypeComponent } from '../../service-type/service-type.component';
import { EnquiresComponent } from 'app/enquires/enquires.component';
import { RequestedcontactsComponent } from 'app/requestedcontacts/requestedcontacts.component';
import { RegistrationComponent } from '../../modals/registration/registration.component';
import { EditUserComponent } from '../../modals/edit-user/edit-user.component';
import { EnquiresViewComponent } from '../../modals/enquires-view/enquires-view.component'
import { DeletemodalComponent } from '../../modals/deletemodal/deletemodal.component';
import { EditPlatformComponent } from '../../modals/edit-platform/edit-platform.component';
import { AddPlatformComponent } from '../../modals/add-platform/add-platform.component';
import { AddServiceComponent } from '../../modals/add-service/add-service.component';
import { DeleteServiceComponent } from '../../modals/delete-service/delete-service.component';
import { EditServiceTypeComponent } from '../../modals/edit-service-type/edit-service-type.component';
import { SmsServicesComponent } from '../../sms-services/sms-services.component';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { MaterialFileInputModule } from 'ngx-material-file-input';
// import { MatFileUploadModule } from 'angular-material-fileupload';
import { NgxStateModule } from 'ngx-state';
import { FileUploadModule ,FileSelectDirective } from 'ng2-file-upload';

import { MatIconModule } from '@angular/material/icon';
import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatDialogModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatCardModule,
  MatSnackBarModule,
  MatRadioModule,
  MatDividerModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatCheckboxModule,
  MatNativeDateModule,
  MatSlideToggleModule,
  MatAutocompleteModule ,
  MatChipsModule,
} from '@angular/material';
import { ExcelService } from 'app/services/excel.service';


@NgModule({
  imports: [
    MatIconModule,
    MaterialFileInputModule,
    FileUploadModule,
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatTableModule,
    MatCardModule,
    MatSortModule,
    MatChipsModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatRadioModule,
    NgxStateModule.forRoot(),
    MatDatepickerModule,
    MatNativeDateModule,
    MatDividerModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatSlideToggleModule ,
    HttpClientModule,
    Ng2TelInputModule,
  ],
  exports: [
    MatSnackBarModule
  ],
  declarations: [
    DashboardComponent,
    PlatformComponent,
    ServiceTypeComponent,
    EnquiresComponent,
    RequestedcontactsComponent,
    DeletemodalComponent,
    RegistrationComponent,
    EditUserComponent,
    EnquiresViewComponent,
    EditPlatformComponent,
    AddPlatformComponent,
    AddServiceComponent,
    DeleteServiceComponent,
    EditServiceTypeComponent,
    SmsServicesComponent,
  ],
  providers: [
    ExcelService
  ],

  entryComponents: [
    EditPlatformComponent,
    AddPlatformComponent,
    DeletemodalComponent,
    RegistrationComponent ,
    EnquiresViewComponent,
    EditUserComponent,
    AddServiceComponent,
    DeleteServiceComponent,
    EditServiceTypeComponent,
  ]
})

export class AdminLayoutModule { }
