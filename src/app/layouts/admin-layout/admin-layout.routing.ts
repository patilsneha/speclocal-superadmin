import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { EnquiresComponent } from 'app/enquires/enquires.component';
import { RequestedcontactsComponent } from 'app/requestedcontacts/requestedcontacts.component';
import { PlatformComponent } from 'app/platform/platform.component';
import { ServiceTypeComponent } from 'app/service-type/service-type.component';
import { SmsServicesComponent } from 'app/sms-services/sms-services.component';

export const AdminLayoutRoutes: Routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    { path: 'dashboard', component: DashboardComponent},
    { path: 'platform', component: PlatformComponent},
    { path: 'smsServices' , component: SmsServicesComponent},
    { path: 'servicetype' , component : ServiceTypeComponent},
    { path: 'requestedContacts', component: RequestedcontactsComponent},
    { path: 'addcredit', component: EnquiresComponent},

];
