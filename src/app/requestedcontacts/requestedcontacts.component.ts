import { Component, OnInit, ViewChild } from '@angular/core';
import * as Chartist from 'chartist';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { MatDialog } from '@angular/material';
import { ViewComponent } from '../modals/view/view.component';
import { DeletemodalComponent } from 'app/modals/deletemodal/deletemodal.component';
import { RegistrationComponent } from '../modals/registration/registration.component';
import { from } from 'rxjs';
import { SpectviewService } from 'app/spectview.service';
import {MatSnackBar} from '@angular/material/snack-bar';
export interface PeriodicElement {
  
  id: number;
  fullname: string;
  // lastname: string;
  email: string;
  contactno: number;
  businessname: string;
  // requirements : string;
  contacttype : string;
}

export const ELEMENT_DATA: PeriodicElement[] = [];
@Component({
  selector: 'app-requestedcontacts',
  templateUrl: './requestedcontacts.component.html',
  styleUrls: ['./requestedcontacts.component.scss']
})
export class RequestedcontactsComponent implements OnInit {

  id: number;
  periodicElement: PeriodicElement;
  disabled = false;
  device: any = [];
  dialogResult = "";
  alluser:any;
  displayedColumns: string[] = ['sn','contacttype', 'fullname','email','contactno', 'businessname'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog, private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer, private service : SpectviewService,
    private _snackBar: MatSnackBar) {

    this.getAllRequestedContacts();
  }
  
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  getAllRequestedContacts(){
    this.service.getAllRequestedContacts().subscribe((res :any)=>{
      console.log(res)
      this.alluser = res.msg;
      console.log("result : " , this.alluser);   
      this.dataSource = new MatTableDataSource(this.alluser);   
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;  
    });
  }
  // tableSearchOption
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnInit() {}
}
                                                                      