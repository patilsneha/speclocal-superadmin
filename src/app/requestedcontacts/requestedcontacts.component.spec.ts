import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestedcontactsComponent } from './requestedcontacts.component';

describe('RequestedcontactsComponent', () => {
  let component: RequestedcontactsComponent;
  let fixture: ComponentFixture<RequestedcontactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestedcontactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestedcontactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
